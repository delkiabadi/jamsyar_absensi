<?php
class Template {
	
	public $header;
	public $sidebar;
	public $content;
	public $footer;
	public $layout;
	public $layout_folder;
	public $obj;
	
	
	public function Template(){
		$this->params = array();
		$this->obj = &get_instance();
		$this->header 			= '_header.php';
		$this->sidebar			= '_sidebar';
		$this->content			= '_content';
		$this->footer			= '_footer';
		$this->layout_folder	= '_layout/';	
	}
	
	public function load($main_view,$data=array(),$flag=TRUE){
		if($flag)
		{
			$this->obj->load->view($this->layout_folder.$this->header);
			$this->obj->load->view($this->layout_folder.$this->sidebar,$data);
			$this->obj->load->view($main_view,$data);
			$this->obj->load->view($this->layout_folder.$this->footer);
		}
		else
		{
			$this->obj->load->view($this->layout_folder.$this->header);
			$this->obj->load->view($main_view,$data);
		}
		
	}
}