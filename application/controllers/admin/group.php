<?php

class Group extends MY_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('common_m');
		$this->load->model('user_m');
		$this->load->helper('pagination');
	}
	
	public function index($page=''){
		if (!checkACL(ACL_VIE)) show_error(ERROR_200);
		
		// PAGING
			$limit = 10;
			$offset = $page =='' ? 0 : ($page-1)*$limit;
					
		
		$res 	= $this->common_m->get_table_where(0,0,'m_kelompok','*',' 1=1');
		$total 	= $res->num_rows();
		$rows	= $this->user_m->get_kelompok($limit,$offset);
			
		$data = array();
		$data['add']	= base_url().'admin/group/tambah';
		$data['hapus']	= 'admin/group/hapus';
		$data['print']	= 'admin/group/print';
		$data['page']	= paging('admin/group/index',$page,$total,$limit);
		$data['rows']	= $rows->result();
		$data['no']		= $offset;
		$this->template->load('admin/group_user/index',$data);	
	}
	public function add(){
		if (!checkACL(ACL_ADD)) show_error(ERROR_200);
	
		$data = array();
		$this->template->load('admin/group_user/tambah',$data,FALSE);			
	}
	public function addAct(){
		if (!checkACL(ACL_ADD)) show_error(ERROR_200);
		$result = true;
		$message = '';
		try
		{
			$this->db->trans_begin();
			$kelompok 	= trim(strtolower($this->getVar('nama_kelompok')));
			$keterangan = $this->getVar('keterangan');
		
			
			// check kelompok jika sudah terdaftar tidak bisa ditambah lagi
			$check = $this->common_m->get_where('m_kelompok',' id_m_kelompok', " lower(kelompok) = '{$kelompok}' ");
			if($check->num_rows() > 0)
				throw new exception(' Kelompok sudah terdaftar!');
			// GET ID TERAKHIR 
			$row = $this->common_m->get_where('m_kelompok',' max(id_m_kelompok)+1 as id', " 1=1 ")->row();	
			$data = array(	'id_m_kelompok'	=> $row->id,
							'kelompok'		=> $kelompok,
							'keterangan' 	=> $keterangan
						);
			$result = $this->common_m->inserted('m_kelompok',$data);
			if(!$result)
				throw new exception('Gagal tambah kelompok!');
			// ADD GROUP TO ACL
				$menus = $this->common_m->get_where('m_menu','*',' 1=1 and parent !=0 ')->result();
				$data = array();	
				foreach($menus as $mn)
				{
					$obj = array(	'id_m_menu'=>$mn->id_m_menu,
									'id_m_kelompok'=>$row->id,
									'permission'=>0);
						array_push($data,$obj);					
				}
				$result = $this->common_m->inserted_batch('m_acl',$data);
				if(!$result)
					throw new exception('Gagal insert kelompok user!');
			$this->db->trans_commit();
			$message = "Proses Berhasil!";
			
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
			echo json_encode(array("status"=>true,"msg"=>$message));	
	}
	public function edit($id_kelompok=''){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$kelompok = $this->common_m->get_where('m_kelompok','*'," id_m_kelompok = {$id_kelompok}")->row();
		
		$data = array();
		$data['row']		= $kelompok;
		$this->template->load('admin/group_user/edit',$data,FALSE);			
	}
	public function editAct(){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$result = true;
		$message = '';
		try
		{
			$id_kelompok = $this->getVar('id_kelompok');
			$kel = $this->common_m->get_where('m_kelompok','*'," id_m_kelompok = {$id_kelompok}")->row();
			
			// cek apakah nama kelompok diganti atau tidak
			// jika diganti check ketersedian kelompok
			$nama_kelompok 	= trim(strtolower($this->getVar('nama_kelompok')));
			$keterangan	= $this->getVar('keterangan');
				
			if($nama_kelompok !== trim(strtolower($kel->kelompok)))
			{
				$check = $this->common_m->get_where('m_kelompok',' id_m_kelompopk', " lower(kelompok) = '{$nama_kelompok}' ");
				if($check->num_rows() > 0)
					throw new exception(' Kelompok sudah terdaftar!');
			}					
			$data = array(	'kelompok'		=>$nama_kelompok,
							'keterangan'	=>$keterangan
							);
			 	
			$result = $this->common_m->updated('m_kelompok',$data,array('id_m_kelompok'=>$id_kelompok));
			if(!$result)
				throw new exception("Gagal update kelompok");
			$message = "Proses Update Berhasil!";	
		}catch(exception $e){
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message));	
	}
	public function hapus(){
		if (!checkACL(ACL_EDT)) show_error(ERROR_200);
		$result = true;
		try {
			$arrID = $this->getVar('usr', TRUE);
			$listID = '';
			foreach ($arrID as $id) {
				
				$listID .= $id.',';
			}
			$listID .= '-1';
			$result = $this->common_m->sql_query("delete from m_kelompok where id_m_kelompok in({$listID})");
			if(!$result)
				throw new exception('Gagal hapus kelompok!');
			$message = 'Proses hapus berhasil!';
		} catch (Exception $e) {
			//Set error status from exception...
			$result = false;
			$message = $e->getMessage();
		}
		echo json_encode(array('status'=>$result,'msg'=>$message));
	}
	public function cetak(){
		// DATA
			$rows 	= $this->common_m->get_table_where(0,0,'m_kelompok','*',' 1=1')->result();

		// REPORT		
		$mainCols 		  = array();

		$arrCol 		  = array();
		$arrCol['title']  = 'NO.';
		$arrCol['width']  = 10;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'R';
		$arrCol['label'] = '1';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'KELOMPOK';
		$arrCol['width']  = 100;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'L';
		$arrCol['label'] = '2';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$arrCol 		  = array();
		$arrCol['title']  = 'KETERANGAN';
		$arrCol['width']  = 200;
		$arrCol['align']  = 'C';
		$arrCol['calign'] = 'R';
		$arrCol['label'] = '3';
		$arrCol['span']   = 2;
		$arrCol['sub']    = null;
		array_push($mainCols, $arrCol);

		$params 			   = array();
		$params['arrHead'] 	   = $mainCols;
		$params['orientation'] = 'P';
		$params['format'] 	   = 'A4';
		$this->load->library('Report', $params);
		
		$this->report->Open();
		$this->report->AddPage();
		
		$no = 1;
		foreach($rows as $row)
		{
			$arrData = array();
			$arrData[] = $no++;
			$arrData[] = strtoupper($row->kelompok);
			$arrData[] = $row->keterangan;
			$this->report->InsertRow($arrData);
		}
			
		$this->report->ShowPDF('kelompok_user_' . time());	
	}	
}