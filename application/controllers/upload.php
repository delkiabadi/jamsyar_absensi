<?php 
class Upload extends MY_Controller{
	var $menu = 'rkap';
	var $sub_menu = 'upload'; 
	var $flag = array('menunggu persetujuan','ditolak','setuju');	
	var $type = array('KONVENSIONAL','SYARIAH');
	var $id_user;
	public function __construct(){
		parent::__construct();
		$this->load->library(array('excel','uploads'));
		$this->load->helper('date');
		$this->load->helper('pagination');
		$this->load->helper('common');
		$this->load->model('common_m');
		$this->load->helper('uang');
		$this->load->model('operasional_m');
		$this->id_user = $this->session->userdata('id_dd_user');
	}
	
	public function index(){	
		$this->template->load('rkap/index',array());
	}
	
	function form(){
		
		$data = array('tahun'=>date('Y'));
		$this->template->load('rkap/form_upload',$data,FALSE);
	}
	
	function uploadAct(){
		$data = $this->uploads->do_upload('file_upload');
		if($data['status'])
		{
			$file_name = $data['upload_file']['file_name'];
			$this->read_file($file_name);
		}	
		else
		{
			print_r($data['upload_file']);
		}	
	}
	
	public function read_file($file_name=''){
		
		$obj = PHPExcel_IOFactory::load('./pub/uploads/'.$file_name);
		$sheetCount = $obj->getSheetCount();
		
		$obj->setActiveSheetIndex(0);
		$maxRow = $obj->getActiveSheet()->getHighestRow();
		$maxColumn = $obj->getActiveSheet()->getHighestColumn();
		$sheet = $obj->getActiveSheet()->toArray(null,true,true,true);
		//print_r($sheet);die('mati');
		//print_r($maxColumn);
		// cek column
		$start_column = 'A';
		$start_row = 11;
		// delete row yang tidak perlu
		for($i=1;$i<$start_row;$i++)
			unset($sheet[$i]);	
		$result= array();
		$key = 0;
		$key_a = 0;
		$key_b = 0;
		$konvensional_arr = array();
		$syariah_arr = array();
		$variabel = 'konvensional';
		foreach($sheet as $row => $columns) {
			
			if($columns['A'] != 15)
			{
				if($columns['A'] != '' && !is_null($columns['A']))
				{
					/*foreach($columns as $column => $data) {
						$result[$key][] = $data;
					}
					$key++;
					*/
					if($columns['A'] == 'A')
						$variabel = 'konvensional';	
					else if($columns['A'] = 'B')
						$variabel = 'syariah';
					$obj = array();
					foreach($columns as $column => $data) {
						//if($column = 'A' || $column == 'B')
							//continue;
						$obj[] = $data;
						
							
					}
					if($variabel == 'konvensional')
						array_push($konvensional_arr,$obj);
					else
						array_push($syariah_arr,$obj);
				}
			}else
			{
				foreach($columns as $column => $data) {
						$result[$key][] = $data;
					}
					$key++;
				break;
			}	
		}
		print_r($konvensional_arr);
		print_r($syariah_arr);
		
		
	
		
		
		//return $result;				
	}
	
}	