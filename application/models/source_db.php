<?php
class Source_db extends CI_Model{
	var $DB;
	public function __construct()
	{
		parent::__construct();
		$this->DB = $this->load->database('destination',TRUE);
	}
	
	// posisi keuangan
	public function realisasi_keuangan($bulan,$tahun)
	{
		$sql ="SELECT
					b.id_m_akun,
					b.nama_akun,
					a.bulan,
					a.tahun,
					a.nominal
				FROM
					t_posisi_keuangan a
				JOIN m_akun b ON b.id_m_akun = a.id_m_akun
				WHERE 
					a.bulan = {$bulan}
					AND a.tahun = {$tahun}
				ORDER BY a.id_m_akun asc		
				";
		
		return $this->DB->query($sql);	
	}
	
	// laba-rugi
	public function laba_rugi($bulan,$tahun)
	{
		$sql ="SELECT
					b.id_m_akun,
					b.nama_akun,
					a.bulan,
					a.tahun,
					a.jumlah as nominal
				FROM
					t_laba_rugi a
				JOIN m_akun b ON b.id_m_akun = a.id_m_akun
				WHERE 
					a.bulan = {$bulan}
					AND a.tahun = {$tahun}
				ORDER BY a.id_m_akun asc		
				";
			
		return $this->DB->query($sql);	
	}
}	