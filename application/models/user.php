<?php
class User extends Model{
	function __construct(){
		parent::__construct();
	}
	public function list_user($limit=0,$offset=0)
	{
		$sql = 'SELECT 
					a.id_m_user,
					a.user_name,
					a.npp,
					a.flag_active,
					b.id_m_kelompok,
					b.kelompok,
					c.id_m_cabang,
					c.nama_cabang,
				FROM 
					m_user a
				JOIN m_kelompok b ON a.id_m_kelompok = b.id_m_kelompok
				LEFT JOIN a.id_m_cabang = c.id_m_cabang	
				';
		if($limit != 0)
			$sql .= " LIMIT {$limit} OFFSET {$offset} ";
		return $this->db->query($sql);		
	}
}
