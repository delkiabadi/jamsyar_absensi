<html>
	<head>
		<title>LOGIN</title>
		<script src="<?=base_url()?>pub/js/jquery.js"></script>
		<link href="<?php echo base_url()?>pub/css/themes/bootstrap.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/font-awesome/css/font-awesome.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/loading.css" rel="stylesheet">
		<link href="<?php echo base_url()?>pub/css/login.css" rel="stylesheet">
	</head>
	<body>
		<div id="box-login">
			<div class="container">
				<form class="login" action="<?=base_url()?>index.php/auth/login" method="post">
					<fieldset>		
				<legend class="legend">Login</legend>
				<div class="input">
					<input type="text" placeholder="Username" required style="width:100%;" name='user_name' />
						<span><i class="fa fa-envelope-o"></i></span>
				</div>
				<div class="input">
					<input type="password" placeholder="Password" required   style="width:100%;" name='pass_word'/>
				<span><i class="fa fa-lock"></i></span>
				</div>
				<button type="submit" class="submit"><i class="fa fa-long-arrow-right"></i></button>
			</fieldset>
		
		<div class="feedback">
			Periksa kembali kombinasi <br />
			Username & password Anda 
		</div>
	</form>
		</div>
	</div>
	</body>
</html>
<script>
$(document).ready(function(){
	$( ".input" ).focusin(function() {
		$( this ).find( "span" ).animate({"opacity":"0"}, 200);
	});

$( ".input" ).focusout(function() {
  $( this ).find( "span" ).animate({"opacity":"1"}, 300);
});

$("form.login").submit(function(e){
	var data	= $(this).serialize();
	var links	= $(this).attr('action');
	$.ajax({
		url 	: links,
		type	: 'post',
		data	: data,
		dataType : 'json',
		success:function(result)
		{
			if(result.status)
			{
				window.location.href = result.link;
			}else
			{	
				var html = result.msg;
				$(".feedback").html(html);
				$(this).find(".submit i").removeAttr('class').addClass("fa fa-check").css({"color":"#fff"});
				$(".submit").css({"background":"#FF0000", "border-color":"#FF0000"});
				$(".feedback").show().animate({"opacity":"1", "bottom":"-80px"}, 400);
				$("input").css({"border-color":"#FF0000"});		
			}				
		},error : function(a,b,c){
			alert(JSON.stringify(a));
			alert(JSON.stringify(b));
			alert(JSON.stringify(c));
		}
	})
	e.preventDefault();
});
})
	
</script>
  

