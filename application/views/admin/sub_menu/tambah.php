<div class="judul tdCenter">
      FORM TAMBAH SUB MENU
</div>
<!-- content -->
<div id="konten">
	<form method="post" id='main_form' class='form_with_ajax' action='<?=base_url()?>admin/submenu/tambahAct'>
		<table class="tabel html_partial" class="display" cellspacing="0" width="100%">	
			<tr>
				<td class='tdRight'>Menu</td>
				<td>
					<select name='menu' class='form-control' required>
						<option value=''>-- Pilih Menu --</option>
						<?php foreach($menu as $mn) { ?>
						<option value='<?php echo $mn->id_m_menu; ?>'> <?php echo $mn->nama_menu;?></option>
						<?php } ?>
					</select>
				</td>
			
			<tr>
				<td class='tdRight'>Sub Menu</td>
				<td><input type='text' name='sub_menu' id='sub_menu' class='form-control' required></td>
			</tr>
			<tr>
				<td class='tdRight'>URL</td>
				<td><input type='text' name='url' class='form-control' required></td>
			</tr>
			 <tr>
				<td>&nbsp;</td>
				<td colspan='3'>
					<input type='submit' value='SIMPAN' class="btn btn-primary" name='submit'>
					<input type='reset' value='RESET' class="btn btn-danger" id='reset'>
				</td>
			 </tr>
		</table>
	</form>
</div>

<script>
$("form#main_form").submit(function(e){
	var link = $(this).attr('action');
	var data = $(this).serialize();
	$.ajax({
		url : link,
		type : 'post',
		data : data,
		dataType : 'json',
		success : function(response){
			if(response.status)
			{
				alert(response.msg);
				opener.location.reload()
				window.close()
			}else
				alert(response.msg);
		},error: function(){
			alert('SESSION ANDA HABIS');
			window.close();
		}
	})
	e.preventDefault();
})
</script>