<div class="row top" style="padding:10px 10px;">
	<b><?php echo strtoupper($pegawai->nama_pegawai); ?></b>
</div>
<form id="submit-form" action="<?=base_url()?>data/edit_act" method="post">
<input type='hidden' name="pegawai_id" value="<?php echo $pegawai->pegawai_id; ?>">
<div id="form_edit">

</div>
</form>
	<div style="margin-top:10px;">
		<table class="tabel cursor" class="display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th width="10px">No.</th>
					<th>TANGGAL</th>
					<th>JAM DATANG</th>
					<th>JAM PULANG</th>
					<th>TELAT</th>
					<th>TIDAK ABSEN DATANG</th>
					<th>TIDAK ABSEN PULANG</th>
					<th>TIDAK HADIR</th>
					<th>LEMBUR</th>
				</tr>
				
			</thead>
			<?php if($total > 0) { 
			
			
				foreach($rows as $row) {
			?>
				<tr class="tr_cursor" onclick="edit(<?=$row->pegawai_id?>,'<?=$row->tanggal?>')">
					<td class="tdCenter"><?php echo ++$no?></td>
					<td class="tdCenter"><?php echo date('d-m-Y',strtotime($row->tanggal));?></td>
					<td class="tdCenter"><?php echo is_null($row->jam_datang) ? '' : date('H:i:s',strtotime($row->jam_datang));?></td>
					<td class="tdCenter"><?php echo is_null($row->jam_pulang) ? '' : date('H:i:s',strtotime($row->jam_pulang));?></td>
					<td><?php echo strtoupper($row->ket_telat);?></td>
					<td><?php echo strtoupper($row->ket_tidak_absen_datang);?></td>
					<td><?php echo strtoupper($row->ket_tidak_absen_pulang);?></td>
					<td><?php echo strtoupper($row->ket_tidak_hadir);?></td>
					<td><?php echo strtoupper($row->ket_lembur);?></td>
				</tr>	
				<?php } } ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="9" style="text-align:right;"><?php echo $page;?></td>
				</tr>
			<tfoot>
		</table>
	</div>
<script>
	function edit(id,tgl)
	{
			
		$.ajax({
			url : '<?=site_url()?>data/get_absen_by_id',
			type : 'post',
			
			data: {id_pegawai : id, tanggal : tgl},
			success : function(html){
				$("#form_edit").html(html);
			},error:function(x,y,z){
				alert(JSON.stringify(x));
			}
		})
	}
	function get_alasan(id)
	{
		var pegawai_id = $("#pegawai_id").val();
		
		$.ajax({
			url : '<?php echo site_url()?>data/get_alasan',
			type : 'post',
			data : {pegawai_id: pegawai_id,keterangan : id},
			success : function(html){
				$("#alasan").html(html);
				$("#surat_dokter").html('');
			},error:function(){
				alert('fail..');
			}	
		})
	}
	
	function get_surat_dokter(id){
		var ket = $("#keterangan").val();	
		var html ='';
		if(id==1 && ket==5)
			html = "<td>Surat Dokter</td><td><select name='surat_dokter'><option value='0'>Tidak Ada</option><option value='1'>Ada</option></td>";
		$("#surat_dokter").html(html);
	}	
	
	$("#submit-form").submit(function(e){
		var data = $(this).serialize();
		var urls = $(this).attr('action');
		$.ajax({
			url : urls,
			type : 'post',
			dataType : 'json',
			data : data,
			success: function(result)
			{
				if(result.status)
				{
					alert(result.msg);
				}else
					alert(result.msg);
			},error : function(x,y,z){
				alert(JSON.stringify(x.responseText));
			}
		})
	e.preventDefault();
})
</script>