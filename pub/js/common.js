function paging(url)
{
	var view;
	$('.form_with_ajax').attr('action',url);
	var form = $('.form_with_ajax');
	$.ajax({
		url : url,
		type : 'post',
		data : form.serialize(),
		async :false,
		success : function(html){
			view = html;
		},error : function(){
			alert('failed..');
		}
	});
	return view;
}


// open pop up window
function openPop(url, name, winWidth, winHeight) {
	var winName, left, top;

	if (typeof(name) == 'undefined')
		winName = 'undefined';
	else
		winName = name;

	if (typeof(winWidth) == 'undefined')
		winWidth = screen.availWidth - 100;

	if (typeof(winHeight) == 'undefined')
		winHeight = screen.availHeight - 100;

	left = (screen.availWidth / 2) - (winWidth / 2);
	top = (screen.availHeight / 2) - (winHeight / 2);

	open(url, winName, "copyhistory=no, status=yes, resizable=yes, toolbar=no, location=no, width=" + winWidth + ",height=" + winHeight + ",left=" + left + ",top=" + top +',scrollbars=yes');
}
/*****************************************************************************/
// SELECT ALL & NOTHING
/*****************************************************************************/

function checkItAll(elm) {
	var arrCheck;
	arrCheck = document.getElementById("tblBody").getElementsByTagName("input");
	for (var i = 0; i < arrCheck.length; i++) {
		if (arrCheck[i].type != "checkbox") continue;
		if (arrCheck[i] != elm) {
			arrCheck[i].checked = elm.checked;
		}
	}
}
function add(){
	var url = get_url()+"add";
	openPop(url, "form add", '500', '500');
}
function tambah(){
	var url = get_url()+"add";
	openPop(url);
}
function print(){
	var url = get_url();
	openPop(url+'cetak','PRINT',800,400);
}